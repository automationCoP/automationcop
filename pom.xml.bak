<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.deloitte.testng.hybrid</groupId>
  <artifactId>TestNG_Hybrid_Framework</artifactId>
  <version>1.0-SNAPSHOT</version>
  <packaging>jar</packaging>

  <name>Testng_Hybrid_Framework</name>
  <url>http://maven.apache.org</url>

  <properties>
    	<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<exec.mainClass>com.deloitte.testng_hybrid_framework.XmlGenerator</exec.mainClass>
  </properties>

  <build>	
  	<resources>
      <resource>
        <directory>src/main/resources</directory>
      </resource>
    </resources>
	<testResources>
		<testResource>
			<directory>src/test/resources</directory>
			<includes>
				<include>log4j.properties</include>	
			</includes>
		</testResource>
	</testResources>
	<plugins>
		<plugin>
			<groupId>org.apache.maven.plugins</groupId>
			<artifactId>maven-surefire-plugin</artifactId>
			<version>2.20.1</version>
			<configuration>
				<suiteXmlFiles>
					<suiteXmlFile>MasterSuite.xml</suiteXmlFile>
				</suiteXmlFiles>
			</configuration>
		</plugin>
		<plugin>			 
    			<groupId>org.apache.maven.plugins</groupId>
    			<artifactId>maven-compiler-plugin</artifactId>
    			<version>3.2</version> <!-- or whatever current version -->
    			<configuration>
      			<source>1.8</source>
      			<target>1.8</target>
    			</configuration>
  		</plugin>

		<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>exec-maven-plugin</artifactId>
				<version>1.2.1</version>
				<executions>
					<execution>
						<goals>
							<goal>java</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<mainClass>com.deloitte.testng_hybrid_framework.XmlGenerator</mainClass>
					<arguments>
						<argument>foo</argument>
						<argument>bar</argument>
					</arguments>
				</configuration>
		</plugin>
	</plugins>
  </build>

 <dependencies>
	<!-- POI -->
	<dependency>
		<groupId>org.apache.poi</groupId>
		<artifactId>poi</artifactId>
		<version>3.16</version>
	</dependency>

	<dependency>
		<groupId>org.apache.poi</groupId>
		<artifactId>poi-ooxml</artifactId>
		<version>3.16</version>
	</dependency>

	<dependency>
		<groupId>org.apache.poi</groupId>
		<artifactId>poi-ooxml-schemas</artifactId>
		<version>3.16</version>
	</dependency>

	<!-- https://mvnrepository.com/artifact/dom4j/dom4j -->
	<dependency>
		<groupId>dom4j</groupId>
		<artifactId>dom4j</artifactId>
		<version>1.1</version>
	</dependency>

	<!-- https://mvnrepository.com/artifact/org.apache.xmlbeans/xmlbeans -->
	<dependency>
		<groupId>org.apache.xmlbeans</groupId>
		<artifactId>xmlbeans</artifactId>
		<version>2.6.0</version>
	</dependency>
	
	<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java -->
 	<dependency>
       		<groupId>org.seleniumhq.selenium</groupId>
        	<artifactId>selenium-java</artifactId>
		<version>3.6.0</version>
   	</dependency>
	
	<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-server -->
	<dependency>
		<groupId>org.seleniumhq.selenium</groupId>
		<artifactId>selenium-server</artifactId>
		<version>3.6.0</version>
	</dependency>

	<!-- https://mvnrepository.com/artifact/com.relevantcodes/extentreports -->
	<dependency>
		<groupId>com.relevantcodes</groupId>
		<artifactId>extentreports</artifactId>
		<version>2.41.2</version>
	</dependency>

	<!-- https://mvnrepository.com/artifact/junit/junit -->
	<dependency>
    		<groupId>junit</groupId>
    		<artifactId>junit</artifactId>
    		<version>4.12</version>
    		<scope>test</scope>
	</dependency>


	<!-- https://mvnrepository.com/artifact/org.testng/testng -->
	<dependency>
		<groupId>org.testng</groupId>
		<artifactId>testng</artifactId>
		<version>6.11</version>
	</dependency>
	
	<!-- https://mvnrepository.com/artifact/org.freemarker/freemarker -->
	<dependency>
    		<groupId>org.freemarker</groupId>
    		<artifactId>freemarker</artifactId>
    		<version>2.3.26-incubating</version>
	</dependency>
	
	<!--  Appium -->
		
	<dependency>
		<groupId>io.appium</groupId>
		<artifactId>java-client</artifactId>
		<version>3.3.0</version>
	</dependency>
	
	<!--gson-->
	<dependency>
		<groupId>com.google.code.gson</groupId>
		<artifactId>gson</artifactId>
		<version>2.8.1</version>
	</dependency>

	<dependency>
		<groupId>org.apache.httpcomponents</groupId>
		<artifactId>httpclient</artifactId>
		<version>4.5.3</version>
	</dependency>

	<dependency>
		<groupId>com.google.guava</groupId>
		<artifactId>guava</artifactId>
		<version>23.0</version>
	</dependency>


	<dependency>
		<groupId>cglib</groupId>
		<artifactId>cglib</artifactId>
		<version>3.2.4</version>
	</dependency>

	<dependency>
		<groupId>commons-validator</groupId>
		<artifactId>commons-validator</artifactId>
		<version>1.6</version>
	</dependency>

	<dependency>
		<groupId>javax.mail</groupId>
		<artifactId>mail</artifactId>
		<version>1.4.7</version>
	</dependency>

	<dependency>
		<groupId>org.jsoup</groupId>
		<artifactId>jsoup</artifactId>
		<version>1.10.3</version>
	</dependency>
	
	<dependency>
		<groupId>org.xerial</groupId>
		<artifactId>sqlite-jdbc</artifactId>
		<version>3.20.0</version>
		<scope>test</scope>
	</dependency>

	<dependency>
		<groupId>org.mongodb</groupId>
		<artifactId>mongodb-driver</artifactId>
		<version>3.5.0</version>
	</dependency>
	
	<!-- https://mvnrepository.com/artifact/org.jooq/joox -->
	<dependency>
    	<groupId>org.jooq</groupId>
    	<artifactId>joox</artifactId>
    	<version>1.5.0</version>
	</dependency>
	
	<!-- https://mvnrepository.com/artifact/net.sourceforge.nekohtml/nekohtml -->
	<dependency>
    	<groupId>net.sourceforge.nekohtml</groupId>
    	<artifactId>nekohtml</artifactId>
    	<version>1.9.22</version>
	</dependency>
	
	<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
	<dependency>
    	<groupId>mysql</groupId>
    	<artifactId>mysql-connector-java</artifactId>
    	<version>6.0.6</version>
	</dependency>
	
	<!-- https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core -->
	<dependency>
    	<groupId>log4j</groupId>
    	<artifactId>log4j</artifactId>
    	<version>1.2.17</version>
	</dependency>
	
	</dependencies>
</project>


