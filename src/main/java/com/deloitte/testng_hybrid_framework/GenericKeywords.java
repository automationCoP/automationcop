package com.deloitte.testng_hybrid_framework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Listeners;

import com.deloitte.testng_hybrid_framework.AXE.AXE;
import com.deloitte.testng_hybrid_framework.captureObjects.CaptureObjects;
import com.deloitte.testng_hybrid_framework.captureObjects.RepositoryGenerator;
import com.deloitte.testng_hybrid_framework.util.Constants;
import com.deloitte.testng_hybrid_framework.util.CurrentDateAndMonth;
import com.deloitte.testng_hybrid_framework.util.ExtentManager;
import com.deloitte.testng_hybrid_framework.util.Listener;
import com.deloitte.testng_hybrid_framework.util.dbConnect2;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


@Listeners(Listener.class)

public class GenericKeywords {
	public WebDriver driver;
	private static final URL scriptUrl = GenericKeywords.class.getClassLoader().getResource("axe.min.js");
	public Properties prop;
	ExtentTest test;
	public Boolean failflag = false;
	public Boolean fatalflag = false;
	private int XPATH_LOCATOR_LIMIT = 16;
	private int XPATH_CSS_NUM = 0;
	boolean elementPresent = false;
	private static final Logger logger = Logger.getLogger(GenericKeywords.class.getName());

	public GenericKeywords(ExtentTest test) {
		this.test = test;
		prop = new Properties();
		try {
			
			// SB - 6/20/2020
			/*
			FileInputStream fs;
			fs = new FileInputStream(System.getProperty("user.dir") + "//src//test//resources//project.properties");
			prop.load(fs);
			logInfo("INFO", "Test = " + test + " Loading project properties file");*/
			// SB - 6/20/2020
			
			// Loading Object Repository
			loadPropertiesFile(Constants.OBJECTREPOSITORY); 
			
			loadPropertiesFile(System.getProperty("user.dir") + "\\src\\test\\resources");
			
		} catch (IOException e) {
			e.printStackTrace();
			logInfo("FATAL", "Test = " + test + " Could not load the project properties file");
		}
	}

//********************************GENERIC FUNCTIONS*****************************************************	
//**********************Open Browser Method*************************************************************
	public String openBrowser(String browserType, String platformType) throws MalformedURLException {
		try {
			prop.put("grid", "N");
			if (prop.getProperty("grid").equals("Y")) {
				logInfo("INFO", "Test = " + test + " Opening " + browserType + " Remote Browser");
				DesiredCapabilities cap = null;
				if (browserType.toUpperCase().equals("MOZILLA")) {
					if (platformType.toUpperCase().equals("WINDOWS")) {
						cap = DesiredCapabilities.firefox();
						cap.setBrowserName("firefox");
						cap.setJavascriptEnabled(true);
						cap.setPlatform(org.openqa.selenium.Platform.WINDOWS);
					} else if (platformType.toUpperCase().equals("LINUX")) {
						cap = DesiredCapabilities.firefox();
						cap.setBrowserName("firefox");
						cap.setJavascriptEnabled(true);
						cap.setPlatform(org.openqa.selenium.Platform.LINUX);
					} else if (platformType.toUpperCase().equals("MAC")) {
						cap = DesiredCapabilities.firefox();
						cap.setBrowserName("firefox");
						cap.setJavascriptEnabled(true);
						cap.setPlatform(org.openqa.selenium.Platform.MAC);
					}
				} else if (browserType.toUpperCase().equals("CHROME")) {
					if (platformType.toUpperCase().equals("WINDOWS")) {
						cap = DesiredCapabilities.chrome();
						cap.setBrowserName("chrome");
						cap.setPlatform(org.openqa.selenium.Platform.WINDOWS);
					} else if (platformType.toUpperCase().equals("LINUX")) {
						cap = DesiredCapabilities.chrome();
						cap.setBrowserName("chrome");
						cap.setPlatform(org.openqa.selenium.Platform.LINUX);
					} else if (platformType.toUpperCase().equals("MAC")) {
						cap = DesiredCapabilities.chrome();
						cap.setBrowserName("chrome");
						cap.setPlatform(org.openqa.selenium.Platform.MAC);
					}
				} else if (browserType.toUpperCase().equals("IE")) {
					cap = DesiredCapabilities.internetExplorer();
					cap.setBrowserName("ie");
					cap.setPlatform(org.openqa.selenium.Platform.WINDOWS);
				} else if (browserType.toUpperCase().equals("EDGE")) {
					cap = DesiredCapabilities.edge();
					cap.setBrowserName("edge");
					cap.setPlatform(org.openqa.selenium.Platform.WINDOWS);
				}
				driver = new RemoteWebDriver(new URL("http://192.168.15.1:4444/wd/hub"), cap);
			} else {
				logInfo("INFO", "Test = " + test + " Opening " + browserType + " local Browser");
				if (browserType.toUpperCase().equals("MOZILLA")) {
					System.setProperty("webdriver.gecko.driver",
							System.getProperty("user.dir") + "//drivers//geckodriver.exe");
					DesiredCapabilities cap = DesiredCapabilities.firefox();
					cap.setCapability("marionette", true);
					driver = new FirefoxDriver(cap);
				} else if (browserType.toUpperCase().equals("CHROME")) {
					System.setProperty("webdriver.chrome.driver",
							System.getProperty("user.dir") + "//drivers//chromedriver.exe");
					driver = new ChromeDriver();
				} else if (browserType.toUpperCase().equals("EDGE")) {
					System.setProperty("webdriver.Edge.driver",
							System.getProperty("user.dir") + "//drivers//MicrosoftWebDriver.exe");
					driver = new EdgeDriver();
				} else if (browserType.toUpperCase().equals("IE")) {

					// SB start change - 6/17/2020 - This code is for the IE browser settings
					// and protected mode settings.
					// System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") +
					// "//drivers//IEDriverServer.exe");
					// driver = new InternetExplorerDriver();

					System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "//drivers//IEDriverServer.exe");	
					DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
					cap.setCapability("nativeEvents", false);
					cap.setCapability("unexpectedAlertBehaviour", "accept");
					cap.setCapability("ignoreProtectedModeSettings", true);
					cap.setCapability("disable-popup-blocking", true);
					cap.setCapability("enablePersistentHover", true);
					cap.setCapability("ignoreZoomSetting", true);
					cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
					InternetExplorerOptions options = new InternetExplorerOptions();
					options.merge(cap);
					driver =  new InternetExplorerDriver(options); 
					// SB end change - 6/17/2020

				}
			}
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			return Constants.PASS;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			logInfo("FATAL", "Test = " + test + " Could not open browser / application: " + browserType);
			return Constants.FATAL + " " + e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "Test = " + test + " Could not open browser / application: " + browserType);
			return Constants.FATAL + " " + e.getMessage();
		}
	}

//*********************************Navigate to URL Method***********************************************
	public String navigate(String urlkey) {
		logInfo("INFO", "Test = " + test + " Navigating to " + prop.getProperty(urlkey));
		try {
			driver.navigate().to(prop.getProperty(urlkey));
			return Constants.PASS;
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "Test = " + test + " Could not navigate to " + prop.getProperty(urlkey));
			return Constants.FATAL + " " + e.getMessage();
		}
	}

//*********************************Click on Element Method**********************************************
	public String click(String locatorkey) {
		logInfo("INFO", "Test = " + test + " Clicking on " + locatorkey);
		try {
			WebElement e = getElement(locatorkey);
			e.click();

			return Constants.PASS;
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "Test = " + test + " Could not click on " + locatorkey);
			return Constants.FATAL + " " + e.getMessage();
		}
	}

//*********************************Input value in Element Method****************************************	
	public String input(String locatorkey, String data) {
		logInfo("INFO", "Test = " + test + " Inputting value in field = " + locatorkey);
		try {
			WebElement e = getElement(locatorkey);
			e.sendKeys(data);

			return Constants.PASS;
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "Test = " + test + " Could not input value in field = " + locatorkey);
			return Constants.FATAL + " " + e.getMessage();
		}
	}

//**********************************Select Value in Drop Down Method************************************
	public String selectDropdown(String locatorkey, String key, String data) {
		logInfo("INFO", "Test = " + test + " Selecting " + data + " in " + locatorkey);
		Select s = null;
		try {
			WebElement m = getElement(locatorkey);
			s = new Select(m);
			if (key.endsWith("Value"))
				s.selectByValue(data);
			else if (key.endsWith("Index"))
				s.selectByIndex(Integer.parseInt(data));
			else if (key.endsWith("VisibleText"))
				s.selectByVisibleText(data);
			return Constants.PASS;
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "Test = " + test + " Could not select " + data + " in " + locatorkey);
			return Constants.FATAL + " Not able to select " + data + "for" + locatorkey;
		}
	}

//*********************************Select value in Check Box Method*************************************
	public String selectCheckbox(String locatorkey) {
		logInfo("INFO", "Test = " + test + " Selecting Checkbox " + locatorkey);
		boolean m = false;
		WebElement e = getElement(locatorkey);
		try {
			m = e.isSelected();
			if (m)
				return Constants.PASS;
			else
				return Constants.FAIL + m + " Checkbox Not selected " + locatorkey;
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "Test = " + test + " Not able to select checkbox " + locatorkey);
			return Constants.FATAL + " Not able to select " + locatorkey;
		}
	}

//****************************MouseOver Method**********************************************************
	public String mouseOver(String locatorkey) {
		logInfo("INFO", "Test = " + test + " Mouseover on " + locatorkey);
		try {
			WebElement m = getElement(locatorkey);
			Actions action = new Actions(driver);
			action.moveToElement(m).build().perform();
			return Constants.PASS;
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "Test = " + test + " failure in finding mouseOverKey " + locatorkey);
			return Constants.FATAL + " failure in finding mouseOverKey " + locatorkey;
		}
	}

//*********************Select Radio Button Method*******************************************************
	public String selectRadioButton(String locatorkey, String value) {
		logInfo("INFO", "Test = " + test + " Selecting radio button " + locatorkey);
		try {
			List<WebElement> e = getElements(locatorkey);
			for (WebElement ele : e) {
				if ((ele.getAttribute("value").equalsIgnoreCase(value)) && (!ele.isSelected())) {
					ele.click();
					logInfo("INFO", "Test = " + test + " Selected radio button " + ele.getAttribute("value"));
					break;
				}
			}
			return Constants.PASS;
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "Test = " + test + " failure in finding RadioButton " + locatorkey + " OR " + value);
			return Constants.FATAL + " failure in finding RadioButton " + locatorkey + " " + value;
		}
	}

//********************************VALIDATION FUNCTIONS**************************************************
//******************************Verify Expected with Element Text Method********************************
	public String verifyText(String locatorkey, String expectedText) {
		logInfo("INFO", "Test = " + test + " Verifying " + expectedText + " with " + locatorkey + ".text");
		try {
			WebElement e = getElement(locatorkey);
			String actualText = e.getText();
			if (actualText.equals(expectedText)) {
				logInfo("PASS", "Test = " + test + " Actual Result is equal to Expected ResultExpected = "
						+ expectedText + " & Actual Result = " + actualText);
				return Constants.PASS;
			} else {
				logInfo("FAIL", "Test = " + test + " Actual Result not equal to Expected Result, Expected = "
						+ expectedText + " & Actual Result = " + actualText);
				return Constants.FAIL + "Actual Result not equal to Expected Result, Expected = " + expectedText
						+ " & Actual Result = " + actualText;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "Test = " + test + " failure in finding " + locatorkey);
			return Constants.FATAL + " failure in finding " + locatorkey;
		}
	}

//******************************Verify Element Present Method*******************************************
	public String verifyElementPresent(String locatorkey) {
		logInfo("INFO", "Test = " + test + " Verifying if " + locatorkey + " element is present");
		boolean result = isElementPresent(locatorkey);
		try {
			if (result) {
				logInfo("PASS", "Test = " + test + " element: " + locatorkey + " is present");
				return Constants.PASS;
			} else {
				logInfo("FAIL", "Test = " + test + " element: " + locatorkey + " is not present");
				return Constants.FAIL + " Not able not find Element - " + locatorkey + ".";
			}
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "Test = " + test + " failure in finding " + locatorkey);
			return Constants.FATAL + " failure in finding " + locatorkey + e.getMessage();
		}
	}

//******************************Verify Element Not Present Method***************************************	
	public String verifyElementNotPresent(String locatorkey) {
		logInfo("INFO", "Test = " + test + " Verifying if " + locatorkey + " element is not present");
		boolean result = isElementPresent(locatorkey);
		try {
			if (!result) {
				logInfo("PASS", "Test = " + test + " element: " + locatorkey + " is not present");
				return Constants.PASS;
			} else {
				logInfo("FAIL", "Test = " + test + " element: " + locatorkey + " is present");
				return Constants.FAIL + " Was able to find Element - " + locatorkey + ".";
			}

		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "Test = " + test + " failure in finding " + locatorkey);
			return Constants.FATAL + " failure in finding " + e.getMessage();
		}
	}

//***************************************UTILITY FUNCTIONS**********************************************
//*********************************Get Element Method***************************************************
	public WebElement getElement(String locatorkey) {
		logInfo("INFO", "Test = " + test + " Getting Element: " + locatorkey);
		WebElement e = null;

		try {
			
			
			if (locatorkey.endsWith("id")) {
				e = driver.findElement(By.id(prop.getProperty(locatorkey)));
			}
			if (locatorkey.endsWith("name")) {
				e = driver.findElement(By.name(prop.getProperty(locatorkey)));
			}
			if (locatorkey.endsWith("xpath") || locatorkey.contains("xpath_")) {
				logInfo("INFO ", "Get Element Locatory Key " + locatorkey);
				e = driver.findElement(By.xpath(prop.getProperty(locatorkey)));
				/*
				 * if (locatorkey.endsWith("xpath") && !checkAppendXPaths(locatorkey)) { String
				 * fileName = System.getProperty("user.dir") +
				 * "//src//test//resources//project.properties"; appendXPathByName(locatorkey,
				 * e, fileName); appendXPathFromPreviousElement(locatorkey, e, fileName);
				 * appendXPathByContains(locatorkey, e, fileName);
				 * appendXPathByChainedDec(locatorkey, e, fileName);
				 * appendXPathByFollowing(locatorkey, e, fileName);
				 * appendXPathByStartsWith(locatorkey, e, fileName);
				 * appendCSSSelectorByTagClassAttribute(locatorkey, e, fileName);
				 * appendCSSSelectorByTagContainingText(locatorkey, e, fileName);
				 * appendCSSSelectorByTagEndingText(locatorkey, e, fileName);
				 * appendCSSSelectorByTagAttrName(locatorkey, e, fileName);
				 * appendCSSSelectorByTagAttrTypeAttrName(locatorkey, e, fileName);
				 * appendCSSSelectorByTagAttrNameAttrContText(locatorkey, e, fileName); }
				 */
			}
			if (locatorkey.endsWith("cssSelector") || locatorkey.contains("CSS_")) {
				e = driver.findElement(By.cssSelector(prop.getProperty(locatorkey)));
			}
			if (locatorkey.contains("xpath_") || locatorkey.contains("CSS_")) {
				System.out.println("inside update");
				String fileName = System.getProperty("user.dir") + "//src//test//resources//project.properties";
				updateElementIdInProperties(locatorkey, e, fileName);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "Test = " + test + " failure in finding " + locatorkey);
		}
		return e;
	}

//**********************************Get Elements Method*************************************************
	public List<WebElement> getElements(String locatorkey) {
		logInfo("INFO", "Test = " + test + " Getting Elements: " + locatorkey);
		List<WebElement> e = null;
		try {
			if (locatorkey.endsWith("id")) {
				e = driver.findElements(By.id(prop.getProperty(locatorkey)));
			}
			if (locatorkey.endsWith("name")) {
				e = driver.findElements(By.name(prop.getProperty(locatorkey)));
			}
			if (locatorkey.endsWith("xpath")) {
				e = driver.findElements(By.xpath(prop.getProperty(locatorkey)));
			}
			if (locatorkey.endsWith("cssSelector")) {
				e = driver.findElements(By.cssSelector(prop.getProperty(locatorkey)));
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "Test = " + test + " failure in finding " + locatorkey);
		}
		return e;
	}

//************************************Is Element Present Method*****************************************
	public boolean isElementPresent(String locatorkey) {
		logInfo("INFO", "Test = " + test + " Verifying if element is present: " + locatorkey);
		List<WebElement> e = null;
		try {
			if (locatorkey.endsWith("id")) {
				e = driver.findElements(By.id(prop.getProperty(locatorkey)));
			}
			if (locatorkey.endsWith("name")) {
				e = driver.findElements(By.name(prop.getProperty(locatorkey)));
			}
			if (locatorkey.endsWith("xpath")) {
				e = driver.findElements(By.xpath(prop.getProperty(locatorkey)));
			}
			if (locatorkey.endsWith("cssSelector")) {
				e = driver.findElements(By.cssSelector(prop.getProperty(locatorkey)));
			}
			if (e.size() == 0)
				return false;
			else
				return true;
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "Test = " + test + " failure in finding " + locatorkey);
			return false;
		}
	}

	// ************************************Get Element Present
	// Method*****************************************
	public int getElementPresent(String locatorkey) {
		logInfo("INFO", "Test = " + test + " Verifying if element is present: " + locatorkey);
		List<WebElement> e = null;

		try {
			if (locatorkey.endsWith("id")) {
				e = driver.findElements(By.id(prop.getProperty(locatorkey)));
			}
			if (locatorkey.endsWith("name")) {
				e = driver.findElements(By.name(prop.getProperty(locatorkey)));
			}
			if (locatorkey.endsWith("xpath") || locatorkey.contains("xpath_")) {
				e = driver.findElements(By.xpath(prop.getProperty(locatorkey)));
			}
			if (locatorkey.endsWith("cssSelector") || locatorkey.contains("CSS_")) {
				e = driver.findElements(By.cssSelector(prop.getProperty(locatorkey)));
			}
			if (e.size() == 0) {
				XPATH_CSS_NUM = XPATH_CSS_NUM + 1;
				for (int i = XPATH_CSS_NUM; i <= XPATH_LOCATOR_LIMIT; i++) {
					if (elementPresent) {
						break;
					}
					String key[] = locatorkey.split("_");
					if (XPATH_CSS_NUM <= 6) {
						locatorkey = key[0] + "_" + "xpath";
					} else {
						locatorkey = key[0] + "_" + "CSS";
					}
					getElementPresent(locatorkey + "_" + i);
				}
			} else {
				elementPresent = true;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "Test = " + test + " failure in finding " + locatorkey);
		}

		return XPATH_CSS_NUM;
	}

//**********************************Get Element Size Method*********************************************
	public int getElementsSize(String locatorkey) {
		logInfo("INFO", " Inputting data in " + prop.getProperty(locatorkey));
		int s;
		List<WebElement> e = getElements(locatorkey);
		try {
			s = e.size();
		} catch (Exception exception) {
			logInfo("FATAL", "Not able to find size of Element " + locatorkey);
			s = 0;
		}
		return s;
	}

//*************************************Window Handler Method********************************************
	public String windowhandle(String locatorkey) {
		logInfo("INFO", " Window handler to switch to child window");
		try {
			Set<String> ids = driver.getWindowHandles();
			Iterator<String> itr = ids.iterator();
			String parentwin = itr.next();
			String childwin = itr.next();
			driver.switchTo().window(childwin);
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			System.out.println("switched to window with title ->" + driver.getTitle());
			return Constants.PASS;
		} catch (Exception exception) {
			/* Assert.fail("failure in switching windows"); */
			exception.printStackTrace();
			logInfo("FATAL", "Not able to find child window " + locatorkey);
			return Constants.FATAL + "Not able to find child window " + locatorkey;
		}
	}

//*************************************Random Number & String Functions*********************************
//*************************************Get Random Number Method*****************************************
	public int getRandomNumber() {
		logInfo("INFO", "Generating Random Number");
		int aNumber = 0;
		try {
			aNumber = (int) ((Math.random() * 90000000) + 10000000);
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "not able to get Random Number");
		}
		return aNumber;
	}

//*************************************Get Random Number Min - Max Method*******************************
	public int getRandomNumber(int min, int max) {
		logInfo("INFO", "Generating Random Number");
		Random rand = new Random();
		int randomNum = 0;
		try {
			randomNum = rand.nextInt((max - min) + 1) + min;
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "not able to get Random Number min & max");
		}
		return randomNum;
	}

	// *************************************Get Random String
	// Method*************************************
	public String getRandomString() {
		logInfo("INFO", "Generating Random String");
		String randomString = null;
		try {
			char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
			StringBuilder sb = new StringBuilder();
			Random random = new Random();
			for (int i = 0; i < 5; i++) {
				char c = chars[random.nextInt(chars.length)];
				sb.append(c);
			}
			randomString = sb.toString();
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "not able to get Random String");
		}
		return randomString;
	}

//*******************************************Close Browser Method***************************************
	public String closeBrowser() {
		logInfo("INFO", "Closing Browser");
		try {
			if (driver != null) {
				driver.quit();
				return Constants.PASS;
			} else
				return Constants.FAIL;
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "not able to close browser");
			return Constants.FATAL + e.getMessage();
		}
	}

//***************************************Thread.Sleep Wait Method***************************************
	public String wait(String timeout) {
		logInfo("INFO", "waiting");
		try {
			Thread.sleep(Integer.parseInt(timeout));
			return Constants.PASS;
		} catch (NumberFormatException n) {
			n.printStackTrace();
			logInfo("FATAL", "exception while waiting");
			return Constants.FATAL + n.getMessage();
		} catch (InterruptedException i) {
			i.printStackTrace();
			logInfo("FATAL", "exception while waiting");
			return Constants.FATAL + i.getMessage();
		}
	}

//************************************LOGGING FUNCTIONS*************************************************
	public void logInfo(String logStatus, String details) {
		if (logStatus.equals("INFO")) {
			test.log(LogStatus.INFO, details);
			logger.info(logStatus + details);
		}
		if (logStatus.equals("FAIL")) {
			test.log(LogStatus.FAIL, details);
			logger.info(logStatus + details);
		}
		if (logStatus.equals("FATAL")) {
			test.log(LogStatus.FATAL, details);
			logger.fatal(logStatus + details);
		}
		if (logStatus.equals("PASS")) {
			test.log(LogStatus.PASS, details);
			logger.info(logStatus + details);
		}
	}

//*******************************Get Page Objects*******************************************************
	public void captureObjects(String url, RepositoryGenerator sitecom) {
		CaptureObjects.getPageObjects(url, sitecom);
	}

//********************************REPORTING FUNCTIONS***************************************************
//********************************Take Screenshot Method************************************************
	public void takeScreenshot(String testName) throws IOException {
		logInfo("INFO", "taking screenshot");
		try {
			// decide name - time stamp
			Date d = new Date();
			String screenshotFile = d.toString().replace(":", "_").replace(" ", "_") + ".png";
			String path = Constants.SCREENSHOT_PATH + testName + "_" + screenshotFile;
			// take screenshot
			File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(srcFile, new File(path));
			// embed
			logInfo("INFO", "attaching screenshot to logs" + test.addScreenCapture(path));
		} catch (Exception exception) {
			exception.printStackTrace();
			logInfo("FATAL", "exception while taking screenshot");
		}
	}

//**********************************Reporting Pass Method***********************************************
	public void reportPass(String testname) throws IOException {
		if (driver != null) {
			if (prop.getProperty("screenshot").equals("Y"))
				takeScreenshot(testname);
		}
		logInfo("PASS", testname + " test passed");
	}

//*********************************Reporting Exception/Fatal Method*************************************
	public void reportException(String testname, String FailureMessage) throws IOException {
		if (driver != null) {
			fatalflag = true;
			takeScreenshot(testname);
		}
		logInfo("FATAL", testname + " The test failed due to " + FailureMessage);
	}

//*********************************Reporting Fail Method************************************************
	public void reportDefect(String testname, String defectMessage) throws IOException {
		if (driver != null) {
			failflag = true;
			takeScreenshot(testname);
		}
		logInfo("FAIL", testname + " Defect Found " + defectMessage);
	}

//*************************************DATABASE FUNCTIONS***********************************************
//*************************************Insert into Detail Table*****************************************
	public void insertDBDetailRecord(String suitename, String tcid, String keyword, String teststatus,
			String testrunkey, String createdby) throws SQLException {
		try {
			logInfo("INFO", "Inserting record in DB Detail table");
			dbConnect2 dbcon = new dbConnect2();
			String insmodel = "INSERT INTO test_detail (test_suite,test_case, test_keyword, test_status, test_runkey,created_by,created_at,modified_by,modified_at)\r\n"
					+ "VALUES (";
			String insmodel2 = ");";
			Timestamp TimeStamp = CurrentDateAndMonth.Timestampval();
			String selInsert = insmodel + "'" + suitename + "'" + "," + "'" + tcid + "'" + "," + "'" + keyword + "'"
					+ "," + "'" + teststatus + "'" + "," + "'" + testrunkey + "'" + "," + "'" + createdby + "'" + ","
					+ "'" + TimeStamp + "'" + "," + "'" + createdby + "'" + "," + "'" + TimeStamp + "'" + insmodel2;
			System.out.println(selInsert);
			dbcon.sqlinsert(selInsert);
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "Not able to connect to DB or Error while inserting records in Detail Table");
		}
	}

	public String selectDBDetailRecord(String suitename, String testrunid, String Status, String keyword) {
		logInfo("INFO", "Selecting record from DB Detail table with run name");
		String testcount = "";
		try {
			testcount = "select count(test_status) AS test_count from test_detail where test_suite = '" + suitename
					+ "'" + "\r\n" + "and test_status = '" + Status + "' and test_keyword = '" + keyword
					+ "' and test_runkey = " + "'" + testrunid + "'" + ";";
			System.out.println(testcount);
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "Not able to connect to DB or Error while selecting records in Detail Table");
		}
		return testcount;
	}

	public String selectDBDetailRecord(String suitename, String testrunid, String testcase, String Status,
			String keyword) {
		logInfo("INFO", "Selecting record from DB Detail table with test case name");
		String stepcount = "";
		try {
			stepcount = "select count(test_status) AS test_count from test_detail where test_suite = '" + suitename
					+ "'" + "\r\n" + "and test_status = '" + Status + "' and test_keyword != '" + keyword
					+ "' and test_runkey = " + "'" + testrunid + "'" + "and test_case = " + "'" + testcase + "'" + ";";
			System.out.println(stepcount);
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "Not able to connect to DB or Error while selecting records in Detail Table");
		}
		return stepcount;
	}

	public void insertDBSummaryRecord(String testrunid, String suitename, String createdBy)
			throws NumberFormatException, SQLException {
		try {
			logInfo("INFO", "Inserting record from test_run_summary table");
			dbConnect2 dbrep = new dbConnect2();
			Timestamp TimeStamp = CurrentDateAndMonth.Timestampval();
			String insmodel = "INSERT INTO test_run_summary (test_runid, test_suite, test_planned, test_passed, test_failed, test_skipped, test_exception, created_by, created_at, modified_by, modified_at)\r\n"
					+ "VALUES (";
			String comma = ",";
			String insmodel2 = ");";
			int passcnt = Integer.parseInt(dbrep.sqlcount(selectDBDetailRecord(suitename, testrunid, "PASS", "n/a")));
			int failcnt = Integer.parseInt(dbrep.sqlcount(selectDBDetailRecord(suitename, testrunid, "FAIL", "n/a")));
			int skipcnt = Integer
					.parseInt(dbrep.sqlcount(selectDBDetailRecord(suitename, testrunid, "SKIPPED", "n/a")));
			int fatalcnt = Integer.parseInt(dbrep.sqlcount(selectDBDetailRecord(suitename, testrunid, "FATAL", "n/a")));

			// Summing up for total test cases
			int testplanned = passcnt + failcnt + skipcnt + fatalcnt;

			// Insert the values
			String selInsert = insmodel + "'" + testrunid + "'" + comma + "'" + suitename + "'" + comma + testplanned
					+ comma + passcnt + comma + failcnt + comma + skipcnt + comma + fatalcnt + comma + "'" + createdBy
					+ "'" + comma + "'" + TimeStamp + "'" + comma + "'" + createdBy + "'" + comma + "'" + TimeStamp
					+ "'" + insmodel2;
			System.out.println(selInsert);
			dbConnect2.sqlinsert(selInsert);
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "Not able to connect to DB or Error while inserting records in test_run_summary table");
		}
	}

	public void insertDBSummaryRecord(String testrunid, String suitename, String testcase, String createdBy)
			throws NumberFormatException, SQLException {
		try {
			logInfo("INFO", "Inserting record from test_case_summary table");
			dbConnect2 dbrep = new dbConnect2();
			Timestamp TimeStamp = CurrentDateAndMonth.Timestampval();
			String insmodel = "INSERT INTO test_case_summary (test_runid, test_suite, test_case, test_steps_planned, test_steps_passed, test_steps_failed, test_steps_skipped, test_steps_exception, created_by, created_at, modified_by, modified_at)\r\n"
					+ "VALUES (";
			String comma = ",";
			String insmodel2 = ");";
			int passcnt = Integer
					.parseInt(dbrep.sqlcount(selectDBDetailRecord(suitename, testrunid, testcase, "PASS", "n/a")));
			int failcnt = Integer
					.parseInt(dbrep.sqlcount(selectDBDetailRecord(suitename, testrunid, testcase, "FAIL", "n/a")));
			int skipcnt = Integer
					.parseInt(dbrep.sqlcount(selectDBDetailRecord(suitename, testrunid, testcase, "SKIPPED", "n/a")));
			int fatalcnt = Integer
					.parseInt(dbrep.sqlcount(selectDBDetailRecord(suitename, testrunid, testcase, "FATAL", "n/a")));

			// Summing up for total test cases
			int teststepsplanned = passcnt + failcnt + skipcnt + fatalcnt;

			// Insert the values
			String selInsert = insmodel + "'" + testrunid + "'" + comma + "'" + suitename + "'" + comma + "'" + testcase
					+ "'" + comma + teststepsplanned + comma + passcnt + comma + failcnt + comma + skipcnt + comma
					+ fatalcnt + comma + "'" + createdBy + "'" + comma + "'" + TimeStamp + "'" + comma + "'" + createdBy
					+ "'" + comma + "'" + TimeStamp + "'" + insmodel2;
			System.out.println(selInsert);
			dbrep.sqlinsert(selInsert);
		} catch (Exception e) {
			e.printStackTrace();
			logInfo("FATAL", "Not able to connect to DB or Error while inserting records in test_case_summary table");
		}
	}

//*********************************updateElementIdInProperties************************************************	
	public void updateElementIdInProperties(String locatorkey, WebElement e, String fileName) {
		logInfo("INFO", "Update old xpathId with new xpathId");
		String elementId = e.getAttribute("id");
		if (elementId == null) {
			return;
		}
		String key[] = locatorkey.split("_");
		locatorkey = key[0] + "_" + "xpath";
		boolean setId = false;
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();

			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey) && !line.contains("#") && !setId) {
					sb.append("    " + locatorkey + " = //*[@id='" + elementId + "']" + "\n");
					setId = true;
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to update old xpathId with new xpathId");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to update old xpathId with new xpathId");
		}
	}

//*********************************appendXPathByName************************************************	
	public void appendXPathByName(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append xpath by Name");
		String elementName = e.getAttribute("name");
		if (e.getAttribute("name") == null || e.getAttribute("name").isEmpty()) {
			return;
		}

		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();
			boolean setName = false;
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey) && !line.contains("#") && !setName) {
					sb.append(line).append("\n");
					sb.append("    " + locatorkey + "_1 = //*[@name='" + elementName + "']" + "\n");
					setName = true;
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by Name for the element Id during first run");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by Name for the element Id during first run");
		}
	}

//*********************************appendXPathFromPreviousElement************************************************	
	public void appendXPathFromPreviousElement(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append xpath by Preceding Element");
		String elementId = e.getAttribute("id");
		if (e.getAttribute("id") == null || e.getAttribute("id").isEmpty()) {
			return;
		}

		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String precedingXPath = "//*[@id='" + elementId + "']/preceding::label[1]";
			WebElement ePreced = driver.findElement(By.xpath(precedingXPath));
			String elementIdPreced = ePreced.getAttribute("id");
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey + "_1") && !line.contains("#")) {
					sb.append(line).append("\n");
					sb.append(
							"    " + locatorkey + "_2 = //*[@id='" + elementIdPreced + "']/following::input[1]" + "\n");
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by Previous element for the element Id during first run");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by Previous element for the element Id during first run");
		}
	}

//*********************************appendXPathByContains************************************************	
	public void appendXPathByContains(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append xpath by Contains");
		if (e.getAttribute("id") == null || e.getAttribute("id").isEmpty()) {
			return;
		}

		String keyword = prop.getProperty("keyword_contains");
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey + "_2") && !line.contains("#")) {
					sb.append(line).append("\n");
					sb.append("    " + locatorkey + "_3 = //input[contains(@id,'" + keyword + "')]" + "\n");
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by Contains keyword during first run");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by Contains keyword during first run");
		}
	}

//*********************************appendXPathByChainedDec************************************************	
	public void appendXPathByChainedDec(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append xpath by Chained Declaration");
		if (e.getAttribute("id") == null || e.getAttribute("id").isEmpty()) {
			return;
		}

		String keyword = prop.getProperty("keyword_contains");
		String elementId = e.getAttribute("id");
		String elementName = e.getAttribute("name");
		String parentDiv = "//input[@id='" + elementId + "']/parent::div";
		WebElement eParentDiv = driver.findElement(By.xpath(parentDiv));
		String parentClass = eParentDiv.getAttribute("class");

		String xpath = null;
		for (int i = 0; i <= 5; i++) {
			xpath = "//div[@class='" + parentClass + "']//input[" + i + "]";
			try {
				WebElement childelement = driver.findElement(By.xpath(xpath));
				if (childelement.getAttribute("name").equals(elementName)
						|| childelement.getAttribute("id").contains(keyword)) {
					break;
				}
			} catch (Exception ex) {

			}

		}

		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey + "_3") && !line.contains("#")) {
					sb.append(line).append("\n");
					sb.append("    " + locatorkey + "_4 = " + xpath + "\n");
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by chained declaration during first run");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by chained declaration during first run");
		}
	}

//*********************************appendXPathByFollowing************************************************	
	public void appendXPathByFollowing(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append xpath by Following");
		if (e.getAttribute("id") == null || e.getAttribute("id").isEmpty()) {
			return;
		}
		String[] elementList = { "input", "label" };
		String elementId = e.getAttribute("id");
		String precedingElementId = null;
		String precedingElementType = null;
		for (int i = 0; i < elementList.length; i++) {
			try {
				String checkPrecedElement = "//input[@id='" + elementId + "']//preceding-sibling::" + elementList[i]
						+ "[1]";
				WebElement precedElement = driver.findElement(By.xpath(checkPrecedElement));
				precedingElementId = precedElement.getAttribute("id");
				precedingElementType = elementList[i];
				break;
			} catch (Exception ex) {

			}
		}

		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey + "_4") && !line.contains("#")) {
					sb.append(line).append("\n");
					sb.append("    " + locatorkey + "_5 = //" + precedingElementType + "[@id='" + precedingElementId
							+ "']//following-sibling::input[1]" + "\n");
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by Following-Sibling during first run");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by Following-Sibling during first run");
		}
	}

//*********************************appendXPathByStartsWith************************************************	
	public void appendXPathByStartsWith(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append xpath by Starts-with");
		if (e.getAttribute("id") == null || e.getAttribute("id").isEmpty()) {
			return;
		}

		String elementId = e.getAttribute("id");

		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey + "_5") && !line.contains("#")) {
					sb.append(line).append("\n");
					sb.append("    " + locatorkey + "_6 = //input[starts-with(@id,'"
							+ elementId.substring(0, elementId.length() / 2) + "')]" + "\n");
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by starts-with during first run");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append XPath by starts-with during first run");
		}
	}

//*********************************appendCSSSelectorByTagClassAttribute************************************************	
	public void appendCSSSelectorByTagClassAttribute(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append CSS Selector by Tag Class Attribute");
		if (e.getAttribute("id") == null || e.getAttribute("id").isEmpty()) {
			return;
		}

		String cls = e.getAttribute("class");
		String elementName = e.getAttribute("name");
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey + "_6") && !line.contains("#")) {
					sb.append(line).append("\n");
					String key[] = locatorkey.split("_");
					locatorkey = key[0] + "_" + "CSS";
					sb.append("    " + locatorkey + "_7 = input." + cls + "[name=" + elementName + "]" + "\n");
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Class Attribute");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Class Attribute");
		}
	}

//*********************************appendCSSSelectorByTagContainingText************************************************	
	public void appendCSSSelectorByTagContainingText(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append CSS Selector by Tag Containing Text");
		if (e.getAttribute("id") == null || e.getAttribute("id").isEmpty()) {
			return;
		}

		String elementId = e.getAttribute("id");
		String elementText = null;
		if (elementId != null && elementId.length() >= 8) {
			elementText = elementId.substring(4, elementId.length() - 4);
		}
		System.out.println("Locator key " + locatorkey);
		String key[] = locatorkey.split("_");
		locatorkey = key[0] + "_" + "CSS";
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey + "_7") && !line.contains("#")) {
					sb.append(line).append("\n");
					sb.append("    " + locatorkey + "_8 = input[id*='" + elementText + "']" + "\n");
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Containing Text");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Containing Text");
		}
	}

//*********************************appendCSSSelectorByTagEndingText************************************************	
	public void appendCSSSelectorByTagEndingText(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append CSS Selector by Tag Ending Text");
		if (e.getAttribute("id") == null || e.getAttribute("id").isEmpty()) {
			return;
		}

		String elementId = e.getAttribute("id");
		String elementText = null;
		if (elementId != null && elementId.length() >= 5) {
			elementText = elementId.substring(5, elementId.length());
		}
		String key[] = locatorkey.split("_");
		locatorkey = key[0] + "_" + "CSS";
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey + "_8") && !line.contains("#")) {
					sb.append(line).append("\n");
					sb.append("    " + locatorkey + "_9 = input[id$='" + elementText + "']" + "\n");
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Ending Text");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Ending Text");
		}
	}

//*********************************appendCSSSelectorByTagAttrName************************************************
	public void appendCSSSelectorByTagAttrName(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append CSS Selector by Tag Attribute Name");
		if (e.getAttribute("id") == null || e.getAttribute("id").isEmpty()) {
			return;
		}

		String elementName = e.getAttribute("name");
		String key[] = locatorkey.split("_");
		locatorkey = key[0] + "_" + "CSS";
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey + "_9") && !line.contains("#")) {
					sb.append(line).append("\n");
					sb.append("    " + locatorkey + "_10 = input[name='" + elementName + "']" + "\n");
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Attribute Name");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Attribute Name");
		}
	}

//*********************************appendCSSSelectorByTagAttrTypeAttrName************************************************	
	public void appendCSSSelectorByTagAttrTypeAttrName(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append CSS Selector by Tag Attribute Type Attribute Name");
		if (e.getAttribute("id") == null || e.getAttribute("id").isEmpty()) {
			return;
		}

		String elementName = e.getAttribute("name");
		String elementType = e.getAttribute("type");
		String key[] = locatorkey.split("_");
		locatorkey = key[0] + "_" + "CSS";
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey + "_10") && !line.contains("#")) {
					sb.append(line).append("\n");
					sb.append("    " + locatorkey + "_11 = input[type='" + elementType + "'][name='" + elementName
							+ "']" + "\n");
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Attribute Type Attribute Name");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Attribute Type Attribute Name");
		}
	}

//*********************************appendCSSSelectorByTagAttrNameAttrContText************************************************	
	public void appendCSSSelectorByTagAttrNameAttrContText(String locatorkey, WebElement e, String fileName) {
		System.out.println("Append CSS Selector by Tag Attribute Name Attribute Containing Text");
		if (e.getAttribute("id") == null || e.getAttribute("id").isEmpty()) {
			return;
		}
		String elementId = e.getAttribute("id");
		String elementName = e.getAttribute("name");
		String elementText = null;
		if (elementId != null && elementId.length() >= 8) {
			elementText = elementId.substring(4, elementId.length() - 4);
		}
		String key[] = locatorkey.split("_");
		locatorkey = key[0] + "_" + "CSS";
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				if (line.contains(locatorkey + "_11") && !line.contains("#")) {
					sb.append(line).append("\n");
					sb.append("    " + locatorkey + "_12 = input[name='" + elementName + "'][id*='" + elementText + "']"
							+ "\n");
					continue;
				}
				sb.append(line).append("\n");
			}
			br.close();
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			bw.append(sb);
			bw.close();
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Attribute Name Attribute Containing Text");
		} catch (IOException fe) {
			fe.printStackTrace();
			logInfo("FATAL", "Not able to append CSS Selector  by Tag Attribute Name Attribute Containing Text");
		}
	}

//*********************************checkAppendXPaths************************************************	
	public boolean checkAppendXPaths(String locatorkey) {
		logInfo("INFO", "Check Append XPath Available in properties");
		if (locatorkey.endsWith("xpath")) {
			if (prop.containsKey(locatorkey + "_1")) {
				return true;
			}
		}
		return false;
	}

	// ***************************************THROW AWAY
	// FUNCTIONS*******************************************
	public String verifyCalc(int data, String locatorkey) {
		logInfo("INFO", "verify calculation method");
		int calc = 2 * 6;
		try {
			if (calc == data)
				return Constants.PASS;
			else {
				return Constants.FAIL + " Calculation is wrong";
			}
		} catch (Exception e) {
			// TODO: handle exception
			logInfo("FATAL", "Not able to find " + locatorkey);
			return Constants.FATAL + "Not able to find " + locatorkey;
		}
	}
	
	
	////
	
	public void loadPropertiesFile(String filePath) throws IOException {
		File file = new File(filePath);
		File[] listOFiles = file.listFiles();
		for (File iterator : listOFiles) {
			if (iterator.isFile() && iterator.getName().contains(".properties")) {
				BufferedReader inputStream = null;
				try {
					inputStream = new BufferedReader(new FileReader(iterator));
					prop.load(inputStream);
					// log("INFO", iterator.getName() + " Loaded");
				} finally {
					if (inputStream != null) {
						inputStream.close();
					}
				}
			}
		}
	} 
	
	// For AXE - Accessibility Functionality - 6/24/2020
	
	/**
     * Description: This method will run AXE @author Tejeshvi @throws
     */
	
	/*
     public void runAXE(String screenName) {
           JSONObject responseJSON = new AXE.Builder(driver, scriptUrl).analyze();
           JSONArray violations = responseJSON.getJSONArray("violations");

           if (violations.length() == 0) {
                 assertTrue("No violations found", true);
           } else {
                 String JSONName = ExtentManager.getJSONName(screenName) + "AXE";
                 JSONName = "JSONFiles/" + JSONName;
                 AXE.writeResults(JSONName, responseJSON);
System.out.println(CDL.toString(violations));
                 
                 try (PrintStream out = new PrintStream(new FileOutputStream(JSONName+".csv"))) {
                     out.print(CDL.toString(violations));
                 }
           }
     }

     /**
     * Description: This method will run the validator @author Tejeshvi @throws
     */
	
	/*
     public void runValidator(String screenName) {
           // validator code
           try {
                 JsonNode response = null;
                 String source = driver.getPageSource();
                 com.mashape.unirest.http.HttpResponse<JsonNode> uniResponse;
                 uniResponse = Unirest.post("http://localhost:8080/vnu").header("User-Agent",
                             "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36")
                             .header("Content-Type", "text/html; charset=UTF-8").queryString("out", "json").body(source)
                             .asJson();
                 response = uniResponse.getBody();
                 String JSONName = ExtentManager.getJSONName(screenName) + "Validator";
                 JSONName = "JSONFiles/" + JSONName;
                 AXE.writeResults(JSONName, response);
                 System.out.println(response);
           } catch (UnirestException e) {
                 // TODO Auto-generated catch block
                 e.printStackTrace();
           }
     }

	*/
	// For AXE - Accessibility Functionality - 6/24/2020
	
	/**
	 * Description: This method will run AXE @author Tejeshvi @throws
	 * @throws FileNotFoundException 
	 */
	public void runAXE(String screenName) throws FileNotFoundException {
		JSONObject responseJSON = new AXE.Builder(driver, scriptUrl).analyze();
		JSONArray violations = responseJSON.getJSONArray("violations");

		if (violations.length() == 0) {
			Assert.assertTrue(true, "No violations found");
		} else {
			
			String JSONName = ExtentManager.getJSONName(screenName) + "AXE";
			JSONName = "JSONFiles/" + JSONName;
			AXE.writeResults(JSONName, responseJSON);
			System.out.println(CDL.toString(violations));
			
			try (PrintStream out = new PrintStream(new FileOutputStream(JSONName+".csv"))) {
			    out.print(CDL.toString(violations));
			}
		}
	}

	/**
	 * Description: This method will run the validator @author Tejeshvi @throws
	 */
	public void runValidator(String screenName) {
		// validator code
		try {
			JsonNode response = null;
			String source = driver.getPageSource();
			com.mashape.unirest.http.HttpResponse<JsonNode> uniResponse;
			uniResponse = Unirest.post("http://localhost:8080/vnu").header("User-Agent",
					"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36")
					.header("Content-Type", "text/html; charset=UTF-8").queryString("out", "json").body(source)
					.asJson();
			response = uniResponse.getBody();
			String JSONName = ExtentManager.getJSONName(screenName) + "Validator";
			JSONName = "JSONFiles/" + JSONName;
			AXE.writeResults(JSONName, response);
			System.out.println(response);
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
