package com.deloitte.testng_hybrid_framework;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Properties;
import org.testng.annotations.Listeners;
import com.deloitte.testng_hybrid_framework.util.Constants;
import com.deloitte.testng_hybrid_framework.util.Listener;
import com.deloitte.testng_hybrid_framework.util.Xls_Reader;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(Listener.class)

public class Keywords {
	AppKeywords appKeywords;
	ExtentTest test;
	String result;
	public Properties prop;

	public Keywords(ExtentTest test) {
		this.test = test;
		prop = new Properties();
		try {
			FileInputStream fs;
			fs = new FileInputStream(System.getProperty("user.dir") + "//src//test//resources//runname.properties");
			prop.load(fs);
			fs.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void executeKeywords(String suitename, String testUnderExecution, Xls_Reader xls,
			Hashtable<String, String> testData) throws IOException, SQLException {
		
		appKeywords = new AppKeywords(test);
		String testRunKey = prop.getProperty("RunName");
		String createdBy = prop.getProperty("CreatedBy");
		
		int allSteps = xls.getRowCount(Constants.KEYWORDS_SHEET);

		for (int rNum = 2; rNum <= allSteps; rNum++) {
			String tcid = xls.getCellData(Constants.KEYWORDS_SHEET, Constants.TCID_COL, rNum);
			if (tcid.equals(testUnderExecution)) {
				String keyword = xls.getCellData(Constants.KEYWORDS_SHEET, Constants.KEYWORD_COL, rNum);
				String object = xls.getCellData(Constants.KEYWORDS_SHEET, Constants.OBJECT_COL, rNum);
				String key = xls.getCellData(Constants.KEYWORDS_SHEET, Constants.DATA_COL, rNum);
				String keyPlatform = xls.getCellData(Constants.KEYWORDS_SHEET, Constants.DATA2_COL, rNum);
				String data = testData.get(key);
				String data2 = testData.get(keyPlatform);
				result = "";
				if (keyword.equals("openBrowser")) {
					result = appKeywords.openBrowser(data, data2);
				} else if (keyword.equals("navigate")) {
					result = appKeywords.navigate(object);
				} else if (keyword.equals("click")) {
					result = appKeywords.click(object);
				} else if (keyword.equals("input")) {
					result = appKeywords.input(object, data);
				} else if (keyword.equals("closeBrowser")) {
					result = appKeywords.closeBrowser();
				} else if (keyword.equals("verifyText")) {
					result = appKeywords.verifyText(object, data);
				} else if (keyword.equals("verifyElementPresent")) {
					result = appKeywords.verifyElementPresent(object);
				} else if (keyword.equals("verifyElementNotPresent")) {
					result = appKeywords.verifyElementNotPresent(object);
				} else if (keyword.equals("EnterTime")) {
					test.log(LogStatus.INFO, "Submitting Time in DTE");
					result = appKeywords.EnterTime(testData);
				} else if (keyword.equals("verifyCalc")) {
					result = appKeywords.verifyCalc(4, object);
				} else if (keyword.equals("filterProductBySearch")) {
					result = appKeywords.filterProductBySearch(testData);
					
				// SB - Testing - 6/20/2020
				}else if (keyword.equals("isGoogleLandingPage")) {
					result = appKeywords.isGoogleLandingPage(testData);
				}else if (keyword.equals("signInPage")) {
					result = appKeywords.signInPage(testData);
				} else if (keyword.equals("isGeicoLandingPage")) {
					result = appKeywords.isGeicoLandingPage(testData);
				}else if (keyword.equals("geicoSignInPage")) {
					result = appKeywords.geicoSignInPage(testData);
				}
				// SB - Testing - 6/20/2020
				
				
				// Lokesh Banga updated #10/08/2017
				// central place reporing failure
				if (result.startsWith(Constants.PASS)) {
					appKeywords.reportPass(tcid);
					appKeywords.insertDBDetailRecord(suitename, tcid, keyword, "PASS", testRunKey, createdBy);
				} else if (result.startsWith(Constants.FATAL)) {
					appKeywords.reportException(tcid, result + "exception found");
					appKeywords.insertDBDetailRecord(suitename, tcid, keyword, "FATAL", testRunKey, createdBy);
				} else if (result.startsWith(Constants.FAIL)) {
					appKeywords.reportDefect(tcid, result + " Assertion Failed");
					appKeywords.insertDBDetailRecord(suitename, tcid, keyword, "FAIL", testRunKey, createdBy);
				}
			}
		}
	}

	public GenericKeywords getGenericKeywords() {
		return appKeywords;
	}
}
