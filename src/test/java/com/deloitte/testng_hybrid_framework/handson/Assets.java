package com.deloitte.testng_hybrid_framework.handson;

public class Assets {
	private String costlyAsssets;

	private String cheapAssets;

	private String getCostlyAsssets() {
		return costlyAsssets;
	}

	private void setCostlyAsssets(String costlyAsssets) {
		this.costlyAsssets = costlyAsssets;
	}

	public String getCheapAssets() {
		return cheapAssets;
	}

	public void setCheapAssets(String cheapAssets) {
		this.cheapAssets = cheapAssets;
	}
	
}
